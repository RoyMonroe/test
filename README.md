# Level I


## Q1
> Расскажите, чем, на ваш взгляд, отличается хорошая верстка от плохой с точки зрения

 * **пользователя**- пользователь не должен замечать хорошую верстку

* **менеджера проекта** - в хорошей верстке менеджер проектов уверен и со спокойствием покажет её клиенту

* **дизайнера** - верстка и её поведение максимально отражает задумку дизайнера

* **верстальщика** - стили описаны максимально понятно, используются общие для компании правила и регламенты

* **клиентского программиста** - компоненты  написаны таким образом, что при перемещении не ломается их логика.

* **серверного программиста** - верстка не нагружает сервер большим кол-вом запросов. 


##Q2
>Опишите основные особенности верстки крупных многостраничных сайтов, дизайн которых может меняться в процессе реализации и поддержки.

Использвание любых методологий, которые унифицируют верстку и уменьшают кол-во усилий для изменения кода.
Можно использовать CSS-in-JS дабы не было проблем с наименованием классов.

>Расскажите о своем опыте верстки подобных сайтов: какие методологии, инструменты и технологии вы применяли на практике.
На практике применял только БЭМ. 


##Q3
>Опишите основные особенности верстки сайтов, которые должны одинаково хорошо отображаться как на любом современном компьютере, так и на смартфонах и планшетах под управлением iOS и Android. Расскажите о своем опыте верстки подобных сайтов: какие инструменты и технологии вы применяли, как проверяли результат на различных устройствах, какие именно устройства требовалось поддерживать.

Особенности: hover на touch-устройствах, viewportwidth,


##Q4
>Расскажите, какие инструменты помогают вам экономить время в процессе написания, проверки и отладки кода.

WebStorm, Nvim - редакторы, всё могут, всё умеют. Eslint, prettier - для проверки и форматирования. Scss-для стилей. Для всего остального Google в помощь, чтобы не придумывать свой велосипед.

##Q5
>Вам нужно понять, почему страница отображается некорректно в Safari на iOS и в IE на Windows. Код писали не вы, доступа к исходникам у вас нет. Ваши действия? Сталкивались ли вы с подобными проблемами на практике?
В таких случаях, нужно использовать кросс-браузерную проверку типа BrowserStack  и любых аналогов.
С подобными проблемами не сталкивался.


##Q6
>Дизайнер отдал вам макет, в котором не показано, как должны выглядеть интерактивные элементы при наведении мыши. Ваши действия?

К базовым элементам можно применить "стандартную" анимацию и использовать основные цвета сайта. Для сложных элементов трясти дизайнера ибо так дела не делаются!


##Q7
>Какие ресурсы вы используете для развития в профессиональной сфере? Приведите несколько конкретных примеров (сайты, блоги и так далее).

В основном всё на youtube'e: IT-KAMASUTRA, Владилен Минин, webDev, Django School,Online Tutorials
Сайты: medium.com, habr.ru

>Какое направление развития вам более близко: JS-программирование, HTML/CSS- верстка или и то, и другое?

 JS-программирование(Логика взаимодействия объектов)
>Какие ещё области знаний, кроме тех, что непосредственно относятся к работе, вам интересны?

Психология отношений



##Q8
Меня зовут Илья. Мне 30 лет. Увлекаюсь хоккеем трейдингом, чтением. Без вредных привычек. Изучаю React,  TypeScript, Python/Django. В планах изучить GraphQL.

Портфолио к сожалению нет.

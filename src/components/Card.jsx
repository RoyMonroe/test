import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
// --------------------------------------------------------------------

const classNames = require("classnames");
const Card = (props) => {
  const [active, setActive] = useState(false);
  const [unchecked, setUnchecked] = useState(false);
  const activeToggle = () => {
    setActive((active) => !active);
    uncheckedFalse();
  };
  const uncheckedTrue = () => {
    setUnchecked(true);
  };
  const uncheckedFalse = () => {
    setUnchecked(false);
  };
  // ------------------Редактирование Title-------------------------
  const [title, setTitle] = useState(props.item.title);
  const [editModeTitle, setEditModeTitle] = useState(false);
  const activateEditMode = () => {
    setEditModeTitle(true);
  };
  const deactivateTitleEditMode = (e) => {
    setEditModeTitle(false);
    setTitle(e.target.value);
  };
  const onTitleChange = (e) => {
    setTitle(e.target.value);
  };
  // -----------------Редактирование SubTitle------------------------
  const [subTitle, setSubTitle] = useState(props.item.subTitle);
  const [editModeSubTitle, setEditModeSubTitle] = useState(false);
  const activateEditModeSubTitle = () => {
    setEditModeSubTitle(true);
  };
  const deactivateSubTitleEditMode = (e) => {
    setEditModeSubTitle(false);
    setSubTitle(e.target.value);
  };
  const onSubTitleChange = (e) => {
    setSubTitle(e.target.value);
  };
  // ----------------Редактирование Portions--------------------------
  const [portions, setPortions] = useState(props.item.properties.portions);
  const [editModePortions, setEditModePortions] = useState(false);
  const activateEditModePortions = () => {
    setEditModePortions(true);
  };
  const deactivatePortionsEditMode = (e) => {
    setEditModePortions(false);
    setPortions(e.target.value);
  };
  const onPortionsChange = (e) => {
    setPortions(e.target.value);
  };
  // ------------------Редактирование PortionsDescription-----------
  const [portionsDescription, setPortionsDescription] = useState(
    props.item.properties.portionsDescription
  );
  const [
    editModePortionsDescription,
    setEditModePortionsDescription,
  ] = useState(false);
  const activateEditModePortionsDescription = () => {
    setEditModePortionsDescription(true);
  };
  const deactivatePortionsDescriptionEditMode = (e) => {
    setEditModePortionsDescription(false);
    setPortionsDescription(e.target.value);
  };
  const onPortionsDescriptionChange = (e) => {
    setPortionsDescription(e.target.value);
  };
  // ------------------Редактирование qtyGifts-------------------------
  const [qtyGifts, setQtyGifts] = useState(props.item.properties.qtyGifts);
  const [editModeQtyGifts, setEditModeQtyGifts] = useState(false);
  const activateEditModeQtyGifts = () => {
    setEditModeQtyGifts(true);
  };
  const deactivateQtyGiftsEditMode = (e) => {
    setEditModeQtyGifts(false);
    setQtyGifts(e.target.value);
  };
  const onQtyGiftsChange = (e) => {
    setQtyGifts(e.target.value);
  };

  // ------------------Редактирование giftDescription----------------
  const [giftDescription, setGiftDescription] = useState(
    props.item.properties.giftDescription
  );
  const [editModeGiftDescription, setEditModeGiftDescription] = useState(false);
  const activateEditModeGiftDescription = () => {
    setEditModeGiftDescription(true);
  };
  const deactivateGiftDescriptionEditMode = (e) => {
    setEditModeGiftDescription(false);
    setGiftDescription(e.target.value);
  };
  const onGiftDescriptionChange = (e) => {
    setGiftDescription(e.target.value);
  };
  // ----------------Редактирование Weight---------------------------
  const [weight, setWeight] = useState(props.item.properties.weight);
  const [editModeWeight, setEditModeWeight] = useState(false);
  const activateEditModeWeight = () => {
    setEditModeWeight(true);
  };
  const deactivateWeightEditMode = (e) => {
    setEditModeWeight(false);
    setWeight(e.target.value);
  };
  const onWeightChange = (e) => {
    setWeight(e.target.value);
  };
  // ----------------Редактирование Description----------------------

  const [description, setDescription] = useState(props.item.description);
  const [editModeDescription, setEditModeDescription] = useState(false);
  const activateEditModeDescription = () => {
    setEditModeDescription(true);
  };
  const deactivateDescriptionEditMode = (e) => {
    setEditModeDescription(false);
    setDescription(e.target.value);
  };
  const onDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  // ---------------------------------------------------------------------
  useEffect(() => {
    setTitle(title);
    setSubTitle(subTitle);
    setPortions(portions);
    setPortionsDescription(portionsDescription);
    setWeight(weight);
    setQtyGifts(qtyGifts);
    setGiftDescription(giftDescription);
    setDescription(description);
  }, [
    title,
    subTitle,
    portions,
    portionsDescription,
    qtyGifts,
    giftDescription,
    weight,
    description,
  ]);

  return (
    <>
      <div className="wrapper-card" key={props.item.id}>
        {props.item.isdisabled === "disabled" ? (
          <article className="product-card  product-card--disabled">
            <div className="product-card__overlay"></div>
            <div className="product-card__content">
              <h5 className="title-default">{props.item.defaultTitle}</h5>
              <h3 className=" title">{props.item.title}</h3>
              <h4 className="sub-title">{props.item.subTitle}</h4>
              <div className="product-card__content__descr">
                <b>{props.item.properties.portions}</b>{" "}
                {props.item.properties.portionsDescription}
                {"\n"}
                <b>{props.item.properties.qtyGifts}</b>{" "}
                {props.item.properties.giftDescription}
              </div>
            </div>
            <div className="product-card__weight">
              {props.item.properties.weight}
              <div className="product-card__weight__text">кг</div>
            </div>
          </article>
        ) : (
          <article
            onClick={activeToggle}
            className={classNames("product-card", {
              "product-card--active": active,
            })}
            onMouseEnter={uncheckedTrue}
            onMouseLeave={uncheckedFalse}
          >
            <div className="product-card__content">
              <h5 className="title-default">
                {active && unchecked ? (
                  <div className="unchk">{props.item.unchecked}</div>
                ) : (
                  props.item.defaultTitle
                )}
              </h5>
              {!editModeTitle ? (
                <h3 onDoubleClick={activateEditMode} className=" title">
                  {title}
                </h3>
              ) : (
                <input
                  onChange={onTitleChange}
                  autoFocus={true}
                  onBlur={deactivateTitleEditMode}
                  value={title}
                />
              )}
              {!editModeSubTitle ? (
                <h4
                  onDoubleClick={activateEditModeSubTitle}
                  className="sub-title"
                >
                  {subTitle}
                </h4>
              ) : (
                <input
                  onChange={onSubTitleChange}
                  autoFocus={true}
                  onBlur={deactivateSubTitleEditMode}
                  value={subTitle}
                />
              )}
              <div className="product-card__content__descr">
                {!editModePortions ? (
                  <>
                    <b onDoubleClick={activateEditModePortions}>{portions}</b>{" "}
                  </>
                ) : (
                  <input
                    onChange={onPortionsChange}
                    autoFocus={true}
                    onBlur={deactivatePortionsEditMode}
                    value={portions}
                  />
                )}
                {!editModePortionsDescription ? (
                  <>
                    {" "}
                    <span onDoubleClick={activateEditModePortionsDescription}>
                      {portionsDescription}
                    </span>
                  </>
                ) : (
                  <input
                    onChange={onPortionsDescriptionChange}
                    autoFocus={true}
                    onBlur={deactivatePortionsDescriptionEditMode}
                    value={portionsDescription}
                  />
                )}
                {"\n"}
                {!editModeQtyGifts ? (
                  <b onDoubleClick={activateEditModeQtyGifts}>{qtyGifts}</b>
                ) : (
                  <input
                    onChange={onQtyGiftsChange}
                    autoFocus={true}
                    onBlur={deactivateQtyGiftsEditMode}
                    value={qtyGifts}
                  />
                )}{" "}
                {!editModeGiftDescription ? (
                  <span onDoubleClick={activateEditModeGiftDescription}>
                    {giftDescription}
                  </span>
                ) : (
                  <input
                    onChange={onGiftDescriptionChange}
                    autoFocus={true}
                    onBlur={deactivateGiftDescriptionEditMode}
                    value={giftDescription}
                  />
                )}
              </div>
            </div>
            <div className="product-card__weight">
              {!editModeWeight ? (
                <span onDoubleClick={activateEditModeWeight}>{weight}</span>
              ) : (
                <input
                  onChange={onWeightChange}
                  autoFocus={true}
                  onBlur={deactivateWeightEditMode}
                  value={weight}
                />
              )}
              <div className="product-card__weight__text">кг</div>
            </div>
          </article>
        )}
        {!props.item.isdisabled ? (
          <div className="wrapper-card__footer">
            {!active ? (
              <>
                {props.item.buy}
                <Link onClick={activeToggle} to="#">
                  купи
                </Link>
              </>
            ) : !editModeDescription ? (
              <span onDoubleClick={activateEditModeDescription}>
                {description}
              </span>
            ) : (
              <input
                onChange={onDescriptionChange}
                autoFocus={true}
                onBlur={deactivateDescriptionEditMode}
                value={description}
              />
            )}
          </div>
        ) : (
          <div className="wrapper-card__footer wrapper-card__footer--disabled">
            Печалька, {props.item.subTitle} закончился.
          </div>
        )}
      </div>
    </>
  );
};

export default Card;

import React from "react";
import "./App.scss";

import Card from './components/Card';

const items = [
  {
    id: "1",
    title: "Нямушка",
    subTitle: " с фуа-гра",
    properties: {
      weight: "0,5",
      portions: "10",
      portionsDescription: "порций",
      qtyGifts: "",
      giftDescription: "мышь в подарок",
    },
    description: "Печень утки разварная с артишоками.",
    defaultTitle: "Сказочное заморское яство",
    unchecked: "Котэ не одобряет?",
    buy:"Чего сидишь? Порадуй котэ, ",
    isdisabled: "",
  },
  {
    id: "2",
    title: "Нямушка",
    subTitle: "с рыбой",
    properties: {
      weight: "2",
      portions: "40",
      portionsDescription: "порций",
      qtyGifts: "2",
      giftDescription: "мыши в подарок",
    },
    description: "Головы щучьи с чесноком да свежайшая сёмгушка.",
    defaultTitle: "Сказочное заморское яство",
    unchecked: "Котэ не одобряет?",
    buy:"Чего сидишь? Порадуй котэ, ",
    isdisabled: "",
  },
  {
    id: "3",
    title: "Нямушка",
    subTitle: " с курой",
    properties: {
      weight: "5",
      portions: "100",
      portionsDescription: "порций",
      qtyGifts: "5",
      giftDescription: "мышей в подарок \n заказчик доволен",
    },
    description: "Филе из цыплят с трюфелями в бульоне.",
    defaultTitle: "Сказочное заморское яство",
    unchecked: "Котэ не одобряет?",
    buy:"Чего сидишь? Порадуй котэ, ",
    isdisabled: "disabled",
  },
];

function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="container__title">Ты сегодня покормил кота?</div>
        {items.map((item)=>
            <Card item={item} key={item.id} />
        )}
       
      </div>
    </div>
  );
}

export default App;
